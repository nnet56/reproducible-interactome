#!/bin/bash

# Author: Marc Melkonian <marc.melkonian@etudiant.univ-rennes1.fr>
# 
# This script is a wrapper around the gen-tstore.sh script that facilitates
# the creation of RDF triplestores for two species: 
#   - Saccharomyces cerevisiae S288C (taxid: 559292),
#   - Homo sapiens (taxid: 9606).
#
# Two triplestores will be created by species: one for heterodimers and one for
# homodimers (equal).

JENA_HOME=~/apache-jena-3.17.0
TS_DIR=~/Inexplicit-rdd/data/triplestores
ONT_DIR=~/Inexplicit-rdd/data/ontologies

TAXIDS=(559292 9606)

for taxid in ${TAXIDS[@]}
do
    echo "Creating triplestore (NA) for: $taxid"
    ./gen-tstore.sh \
        --jena_home "${JENA_HOME}/bin/" \
        --loc "${TS_DIR}/db_${taxid}_NA" \
        --apid "${ONT_DIR}/apid-NA-${taxid}.ttl" \
        --bin "${ONT_DIR}/apid-S1-bin.ttl" \
        --mi "${ONT_DIR}/mi.owl" 

    echo "Creating triplestore (NA,equal) for: $taxid"
    ./gen-tstore.sh \
        --jena_home "${JENA_HOME}/bin/" \
        --loc "${TS_DIR}/db_${taxid}_equal_NA" \
        --apid "${ONT_DIR}/apid-equal-NA-${taxid}.ttl" \
        --bin "${ONT_DIR}/apid-S1-bin.ttl" \
        --mi "${ONT_DIR}/mi.owl" 
done
