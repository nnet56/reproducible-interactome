#!/bin/bash

# Author: Marc Melkonian <marc.melkonian@etudiant.univ-rennes1.fr>
# 
# This script creates a RDF triplestore based on the Apache Jena suite.
# 
# Inputs:
#    - jena_home : Apache Jena bin directory
#    - loc       : triplestore location
#    - apid      : APID ttl file
#    - bin       : APID binary IDMs ttl file
#    - mi        : PSI-MI OWL file
#
# Output:
#   - The directory specified with `loc` containing the triplestore.

LOADER_EXE='tdb2.tdbloader'

function usage() {
    echo "create-apid-tstore.sh"
    echo "  -h          : show usage"
    echo "  --jena_home : Apache Jena bin directory"
    echo "  --loc       : triplestore location"
    echo "  --apid      : APID ttl file"
    echo "  --bin       : APID binary IDMs ttl file"
    echo "  --mi        : PSI-MI OWL file"
    exit 1
}

function exit_on_error() {
    echo "$1"
    exit 1
}

OPTS=$( getopt -o h -l jena_home:,loc:,apid:,bin:,mi: -- "$@" )

if [[ $# -eq 0 ]]
then
    usage
    exit 1
fi

eval set -- "$OPTS"

while true
do
    case "$1" in
        -h)          usage; exit 0;;
        --jena_home) JENA_HOME="$2"; shift 2;;
        --loc)       LOC="$2"; shift 2;;
        --apid)      APID="$2"; shift 2;;
        --bin)       BIN="$2"; shift 2;;
        --mi)        MI="$2"; shift 2;;
        --)          shift 2; break;;
    esac
done

# Arguments testing
if [[ ! -d "$JENA_HOME" ]]; then
     exit_on_error "Directory not found: '$JENA_HOME'"
fi

if [[ ! -f "${JENA_HOME}/${LOADER_EXE}" ]]; then
     exit_on_error "Loader not found: '${JENA_HOME}/${LOADER_EXE}'"
fi

if [[ ! -f "$APID" ]]; then
     exit_on_error "APID ttl file not found: '$APID'"
fi

if [[ ! -f "$BIN" ]]; then
     exit_on_error "APID binary ttl file not found: '$BIN'"
fi

if [[ ! -f "$MI" ]]; then
     exit_on_error "PSI-MI OWL file not found: '$MI'"
fi

# Remove triplestore if exists
echo "Removing triplestore if exists…"
rm -r "$LOC"

# Create triplestore
${JENA_HOME}/${LOADER_EXE} \
    --loc=$LOC \
    $APID \
    $BIN \
    $MI
