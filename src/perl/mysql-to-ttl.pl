#!/usr/bin/env perl

# mysql-to-ttl.pl
#
# Author: Marc Melkonian <marc.melkonian@etudiant.univ-rennes1.fr>
# Created: Wed 29 Apr 2020
#
# This script converts APID mitab files into an ontology in turtle format,
# following the BioPAX level 3.
# Beware that conversion of heterodimers and homodimers have to be done
# separately - thus with generation of two different files - with the
# --sameprots flag.
#
# Usage :
# -----
# mysql-to-ttl.pl [--filter] \
#                 [--sameprots] \
#                 --taxid <(9606|559292)> \
#                 --out <filename> \
#                 --db <MySQL database> \
#                 --host <MySQL hostname>
#
# Options:
# -------
#   --filter    filter out APID's "Not Assigned" (NA) interaction detection
#               methods. If active, suffix '-NA' will be added to the output
#               filename
#   --sameprots only generate homodimers
#   taxid       taxonomy identifier: 9606 (Human) or 559292 (Yeast)
#   out         output filename
#   db          MySQL database name
#   host        MySQL connection host

use strict;
use warnings;

use DBI;
use DBD::mysql;
use File::Copy "cp";
use File::Spec;
use File::Basename qw(dirname basename);
use Data::Dumper;
use Getopt::Long;
use Term::ReadKey;

# CLI Options
my %Opt = (
    filter    => 0,          # Default: no filtering         
    sameprots => 0,          # Default: export heterodimers
    outfile   => '',         # output filename                      
    taxid     => '',         # taxonomy identifier from mitab files
    db        => '',         # MySQL database name              
    host      => 'localhost' # MySQL hostname                      
);

# Helper to manage output filename
sub ofile_opt_handler {
    my ($optname, $optval) = @_;
    my $dirname = dirname($optval);
    my $filename = basename($optval,  ".ttl");
    $Opt{outfile} = $dirname . '/' . $filename;

    # append suffix if NA idms are filtered
    if ( $Opt{filter} && ($Opt{outfile} !~ m/-NA/) ) {
        $Opt{outfile} .= '-NA';
    }
    $Opt{outfile} .= '.ttl';
}

GetOptions(
    'filter!'    => \$Opt{filter},
    'sameprots!' => \$Opt{sameprots},
    'out=s'      => \&ofile_opt_handler,
    'taxid=i'    => \$Opt{taxid},
    'db=s'       => \$Opt{db},
    'host=s'     => \$Opt{host},
);
 
# Helper to exit if CLI arguments are missing
sub exit_on_error {
    print "usage: mysql-to-ttl.pl [--filter] [--sameprots] "
    print "--taxid <(9606|559292)> ";
    print "--db <database> ";
    print "[--host <localhost (Default)>] ";
    print "--out <filename>\n";
    exit 1;
}

if (!$Opt{taxid}
    || !$Opt{outfile}
    || !$Opt{db}) {
    exit_on_error();
}

print "Enter MySQL '$Opt{db}' database password: ";
Term::ReadKey::ReadMode('noecho');
$Opt{pass} = <>;
chomp($Opt{pass});
Term::ReadKey::ReadMode('restore');
print "\n";

if ($Opt{filter}) {
    print "Filtering: On\n";
} else {
    print "Filtering: Off\n";
}

# I/O files
# SPARQL preambule file
my $preambule_file = './preambule.rq';

print "Output file: " . File::Spec->rel2abs($Opt{outfile}) . "\n";
open(my $OFH, ">", $Opt{outfile})
    or die "Can't open < $Opt{outfile}: $!";

# Copy preambule into output file
cp($preambule_file, $OFH);

# DBI (MySQL) connection options
my $dbh = DBI->connect(
    "dbi:mysql:$Opt{db};host=$Opt{host}",
    $Opt{db},
    $Opt{pass},
    { RaiseError => 1 },
) or die $DBI::errstr;


# Generate the APID database triples

print "Writing: databases…\n";
print $OFH "### Source databases\n\n";

# Hand-written APID source database
my $out = <<EOF;
:pubmed_30715274 rdf:type bp:PublicationXref ,
                          OWL:NamedIndividual ;
                 bp:id "30715274"^^xsd:string ;
                 bp:title "APID database: redefining protein-protein interaction experimental evidences and binary interactomes."^^xsd:string ;
                 bp:db "PUBMED"^^xsd:string .

:APID_database rdf:type bp:Provenance ,
                        OWL:NamedIndividual ;
               bp:standardName "APID"^^xsd:string ;
               bp:displayName "APID database (http://cicblade.dep.usal.es:8080/APID/init.action)"^^xsd:string ;
               bp:xref :pubmed_30715274 .

EOF

my $q = qq( SELECT * FROM apid_source_database; );

my $sth = $dbh->prepare($q);
my $rv = $sth->execute();
if ($rv < 0) {
    print $DBI::errstr;
}

while (my $r = $sth->fetchrow_hashref) {
	$out .= <<EOF;
:$r->{name} rdf:type bp:Provenance ,
                        OWL:NamedIndividual ;
               bp:standardName "$r->{name}"^^xsd:string .

EOF

}

print $OFH $out;


# Generate taxonomies triplets

print "Writing: taxonomies…\n";
print $OFH "### Taxonomy\n\n";

$q = qq( SELECT * FROM apid_taxonomy; );

$sth = $dbh->prepare($q);
$rv = $sth->execute();
if ($rv < 0) {
    print $DBI::errstr;
}

while (my $r = $sth->fetchrow_hashref) {
    $out = <<EOF;
:taxon_$r->{taxid} rdf:type bp:UnificationXref ,
                     OWL:NamedIndividual ;
            bp:id "$r->{taxid}"^^xsd:string ;
            bp:db "taxonomy"^^xsd:string .

:BioSource_$r->{taxid} rdf:type bp:BioSource ,
                       OWL:NamedIndividual ;
              bp:displayName "$r->{name}"^^xsd:string ;
              bp:xref :taxon_$r->{taxid} .

EOF

    print $OFH $out;
}


# Generate proteins triplets
#
# Each protein needs three objects in BioPAX:
#   - a bp:Protein object,
#   - a reference to this protein object, of type bp:ProteinReference,
#   - the source of this protein, of type bp:UnificationXref.

print "Writing: proteins…\n";
print $OFH "### Proteins\n\n";

$q = qq( SELECT * FROM apid_protein WHERE taxid = ?; );

$sth = $dbh->prepare($q);
$rv = $sth->execute($Opt{taxid});
if ($rv < 0) {
    print $DBI::errstr;
}


while (my $r = $sth->fetchrow_hashref) {
    my $prefix = uc($r->{db_prefix});
    my $alias_ident = $r->{alias_ident};
    my $tmp = $alias_ident =~ s/"//g;
    $alias_ident = $tmp;
    my $alias_name_type = $r->{alias_name_type};
    $out = <<EOF;
:${prefix}_$r->{ident} rdf:type bp:UnificationXref ,
                         OWL:NamedIndividual ;
                bp:id "$r->{ident}"^^xsd:string ;
                bp:db "$prefix"^^xsd:string .

:ProteinReference_$r->{ident} rdf:type bp:ProteinReference ,
                             OWL:NamedIndividual ;
                    bp:displayName "$r->{ident}"^^xsd:string ;
                    bp:standardName "${alias_ident}(${alias_name_type})"^^xsd:string ;
                    bp:organism :BioSource_$r->{taxid} ;
                    bp:xref :${prefix}_$r->{ident} .

:$r->{ident} rdf:type bp:Protein ,
               OWL:NamedIndividual ;
      bp:displayName "$r->{ident}"^^xsd:string ;
      bp:standardName "${alias_ident}(${alias_name_type})"^^xsd:string ;
      bp:entityReference :ProteinReference_$r->{ident} ;
      bp:xref :${prefix}_$r->{ident} .

EOF

    print $OFH $out;
}


# Generate publications identifiers

print "Writing: publications identifiers…\n";
print $OFH "### Publications identifiers\n\n";

$q = qq( SELECT * FROM apid_publication_identifier; );

$sth = $dbh->prepare($q);
$rv = $sth->execute();
if ($rv < 0) {
    print $DBI::errstr;
}

#   bp:author "$r->{author}"^^xsd:string .
while (my $r = $sth->fetchrow_hashref) {
    $out = <<EOF;
:$r->{prefix}_$r->{code} rdf:type bp:PublicationXref ,
                          OWL:NamedIndividual ;
                 bp:id "$r->{code}"^^xsd:string ;
                 bp:db "$r->{prefix}"^^xsd:string .

EOF

    print $OFH $out;
}


# Generate interactions detections methods

print "Writing: interactions detections methods…\n";
print $OFH "### Interactions detections methods\n\n";

$q = qq( SELECT * from apid_interaction_detection_method; );

$sth = $dbh->prepare($q);
$rv = $sth->execute();
if ($rv < 0) {
    print $DBI::errstr;
}

while (my $r = $sth->fetchrow_hashref) {
	my $prefix = uc($r->{prefix});
	my $inter_det_meth = $prefix . '_' . $r->{code};
        my $spc_name = $r->{spc_name};
        $spc_name =~ s/"/\\"/g;
        my $name = $r->{name};
        $name =~ s/[()\/"]/_/g;
# Below is left out because mi.owl already referenced these methods
#:$inter_det_meth rdf:type bp:UnificationXref ,
#                      OWL:NamedIndividual ;
#             bp:id "MI:$r->{code}"^^xsd:string ;
#             bp:db "$prefix"^^xsd:string .
    $out = <<EOF;

:${name}_ rdf:type bp:EvidenceCodeVocabulary ,
                                 OWL:NamedIndividual ;
                        bp:term "$spc_name"^^xsd:string ;
                        bp:xref obo:MI_$r->{code} .

:$name rdf:type bp:Evidence ,
                                OWL:NamedIndividual ;
                       bp:comment ""^^xsd:string ;
                       bp:evidenceCode :${name}_ .

EOF

    print $OFH $out;
}


# Generate interactions identifiers and PPIs

print "Writing: PPIs…\n";
print $OFH "### PPIs\n\n";

$q = qq(
SELECT
    REPLACE(ar.publication_identifiers, ':', '_') AS pub_ident,
    ap1.ident AS prot1,
    ap2.ident AS prot2,
    aidm.spc_name AS idm_spc_name,
    aidm.name AS idm_name,
    asdb.name AS sdb_name
FROM apid_raw AS ar
LEFT JOIN apid_protein AS ap1
    ON ap1.txt_raw = ar.id_interactor_a
LEFT JOIN apid_protein AS ap2
    ON ap2.txt_raw = ar.id_interactor_b
LEFT JOIN apid_interaction_detection_method AS aidm
    ON aidm.txt_raw = ar.interaction_detection_method
LEFT JOIN apid_source_database AS asdb
    ON asdb.txt_raw = ar.source_database
WHERE ar.taxid_interactor_a LIKE '%$Opt{taxid}%'
);

if ($Opt{filter}) {
        $q .= qq(
AND ar.interaction_detection_method NOT REGEXP 
        'MI:0000|MI:0001|MI:0013|MI:0045|MI:0362|MI:0363|MI:0364|MI:0492|MI:0493|MI:0686|MI:1088'
        );
}

if ($Opt{sameprots}) {
        print "Same\n";
    $q .= qq(
    AND ar.id_interactor_a = ar.id_interactor_b
    );
}

$q .= ';';

$sth = $dbh->prepare($q);
$rv = $sth->execute();
if ($rv < 0) {
    print $DBI::errstr;
}

# row number to disambiguate PPI between same pair and same idm
my $i = 0;
while (my $r = $sth->fetchrow_hashref) {
        $i++;

        #my $idm_name = $r->{idm_name};
        # $idm_name =~ s/"//g;
        # $idm_name =~ s/\//_/g;
        #$idm_name =~ s/[()\/"]/_/g;
        my $idm_spc_name = $r->{idm_spc_name};
        $idm_spc_name =~ s/"/\\"/g;
        #$idm_spc_name =~ s/_/ /g ;

        # to include confidence scores, which are not currently used by APID,
        #  use these lines instead.
        #     bp:evidence :${idm_name} ,
        #                 :confidence_score ;
        #     bp:xref :$inter_id ;

    $out = <<EOF;
:$r->{prot1}_$r->{prot2}_by_$r->{idm_name}_$i rdf:type bp:MolecularInteraction ,
                                            OWL:NamedIndividual ;
                                   bp:availability "!!! TO COMPLETE !!!"^^xsd:string ;
                                   bp:displayName "{$r->{prot1}, $r->{prot2}} by $idm_spc_name"^^xsd:string ;
                                   bp:datasource :$r->{sdb_name} ;
                                   bp:evidence :$r->{idm_name} ;
                                   bp:xref :$r->{pub_ident} ;
EOF

    if ($Opt{sameprots}) {
        $out .= <<EOF;
                                   bp:participant1 :$r->{prot1} ;
                                   bp:participant2 :$r->{prot2} .
EOF

   }
   else {
        $out .= <<EOF;
                                   bp:participant :$r->{prot1} ;
                                   bp:participant :$r->{prot2} .
EOF

   }


    print $OFH $out;
}

print "Total PPIs processed: $i\n";

#
## Generate confidence score
## Note: since this entry is empty in APID, we hard-code it.
#
#print "Writing: confidence scores…\n";
#print $OFH "### Confidence scores\n\n";
#
#$out = <<EOF;
#:confidence_score rdf:type bp:Evidence ,
#                           OWL:NamedIndividual ;
#                  bp:confidence :confidence_Score_- .
#EOF
#
#print $OFH $out;
close $OFH;

print "Done!\n";

$dbh->disconnect;
