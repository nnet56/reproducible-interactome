#!/bin/bash

# convert_apid.sh
#
# Author: Marc Melkonian <marc.melkonian@etudiant.univ-rennes1.fr>
#
# A wrapper script to generate APID-converted ontologies for yeast and human.

TAXIDS=( 559292 9606 )

for taxid in ${TAXIDS[@]}
do
    echo "Converting apid (NA) ${taxid}"
    ./mysql-to-ttl.pl \
            --filter \
            --taxid $taxid \
            --db mln \
            --host genobdd \
            --out ~/Inexplicit-rdd/data/ontologies/apid-NA-${taxid}.ttl

    echo "Converting apid (NA,equal) ${taxid}"
    ./mysql-to-ttl.pl \
            --filter \
            --sameprots \
            --taxid $taxid \
            --db mln \
            --host genobdd \
            --out ~/Inexplicit-rdd/data/ontologies/apid-equal-NA-${taxid}.ttl
done
