#!/usr/bin/env perl

# apid-suppl-binary.pl
#
# Author: Marc Melkonian <mm@asm35.info>
# Created: Tue 30 Jun 2020
#
# This script convert the APID table S1 available as a supplementary data of
# Alonso-López, 2019 (1) at (2).
# What is exported is APID classification of interaction detection methods as
# binary or indirect.
#
# (1) https://pubmed.ncbi.nlm.nih.gov/30715274/
# (2) https://oup.silverchair-cdn.com/oup/backfile/Content_public/Journal/database/2019/10.1093_database_baz005/2/database_alonsoetal2018-supplementary-table_s1_baz005.xlsx?Expires=1613919432&Signature=FzUTMyfjPWYKUCSKGj~v2o5p0CCYgvXCmi2vOQS8Z8OPvWuNbzKGOA0ey8f~6SVFxjWFltZ9jseNm-gQloGt~m7AkkKdo8M8nXutGfA62SL8Sj3Ewh3xbKis-P5OxcCCbb~xbnAb3BovzvmzAE-UoON99JFH56ijYhuTULLBAeW5xm8bG-Dz7YkeMZsRIz6rPCob6TELEGki2jSabAzOlyRioiLetBT3P1ymTIy2CJNuAXZhJfRnSxwTGSA1EWbYaGKdSccy6h2u71vzu8~04xxWSkZlZTveowojOxYDHDHn7DTQJ99ncUJB5vsdm-Hs24UDDtPBIAeOk4IOA2-0Nw__&Key-Pair-Id=APKAIE5G5CRDK6RD3PGA
#
# Usage:
# -----
# apid-bin-to-ttl.pl [--sep <char>] --in <file> --out <file>
#
# Options:
# -------
#   --sep       CSV fields character delimiter
#   --in        CSV input file
#   --out       output file

use strict;
use warnings;

use File::Basename;
use Text::CSV;
use Getopt::Long;

my $ifile = '';   # input filename
my $ofile = '';   # output filename
my $sep   = ',';  # CSV field delimiter


# CLI options

GetOptions(
    'in=s'   => \$ifile,
    'out=s'  => \$ofile,
    'sep=s'  => \$sep
);


# Main

if (!$ifile || !$ofile) {
    print "usage: apid-bin-to-ttl.pl --in <input file> --out <output file>\n";
    exit 1;
}

my $csv = Text::CSV->new({
    binary    =>  1,
    sep_char  =>  $sep,
    eol       =>  $/
}) or die "Cannot use CSV: " . Text::CSV->error_diag();

open(my $FH, "<", $ifile)
    or die "Can't open < $ifile: $!";

open(my $OFH, ">", $ofile)
    or die "Can't open < $ofile: $!";

$csv->column_names([
    "psi_mi_id",
    "psi_mi_id_num",
    "psi_mi_terms",
    "method_type",
    "bin_uni_det_method",
    "mi_id_bin_uni_det_method",
    "cur_evts_num_apid_all",
    "cur_evts_num_apid_hum"
]);

# SPARQL preambule
my $str = <<EOF;
\@prefix : <http://www.biopax.org/examples/myExample#> .
\@prefix obo: <http://purl.obolibrary.org/obo/> .
\@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .

EOF

print $OFH $str;

# Parse file
while (my $line = $csv->getline_hr($FH)) {
    my $tmp_id = $line->{psi_mi_id};
    $tmp_id =~ s/:/_/;
    my $id = 'obo:' . $tmp_id;

    my $is_binary = 'true';
    if ( $line->{method_type} ne 'binary' ) {
        $is_binary = 'false';
    }

    $str = "$id :apid_suppl_is_binary \"$is_binary\"^^xsd:boolean .\n";
    print "$str";

    print $OFH $str;
}

close $OFH;
print "Done!\n";
