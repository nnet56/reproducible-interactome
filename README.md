This file describes the step-by-step process to assess redundancy and generate a
redundancy-free interactome from primary data aggregated by the APID
meta-database.

1. Import APID's mitab files into MySQL

APID mitab files are the primary data needed and are available at
[APID website](http://cicblade.dep.usal.es:8080/APID/init.action#subtab4).

Currently, mitab files of two species are supported:
 - _Homo sapiens_ (taxid: 9606),
 - _Saccharomyces cerevisiae S288C_ (taxid: 559292).

Note that the two files are mandatory to further use wrapper scripts that
facilitate the generation of triplestores.

To import data into MySQL, run: _src/MySQL/init-apid-db.mysql_.

2. Ontologies

## PSI-MI

The PSI-MI ontology of Molecular Interactions is directly available in OWL
format [here](http://ontologies.berkeleybop.org/mi.owl).

## APID

+ Conversion of APID data (from mitab) into an ontology.

This is done with the script: _src/perl/mysql-to-ttl.pl .

Note that the generations of data for heterodimers and homodimers have to be
done separately (see the script help for more info).

+ Conversion of APID supplementary data (for binary interaction detection
  methods)

Run: _/src/perl/apid--bin-to-ttl.pl_.

## RDF triplestore

### Triplestores creation

Once all ontologies have been downloaded or generated, they will be merged in
RDF triplestores.

A wrapper script is located at: _src/jena/create-tstore.sh_.

### SPARQL queries

The triplestores can then be queried.
Helper scripts to run queries stored in text files are available at:
_src/sparql/_ (_sparql.sh_ and _sparql-equal.sh_).

## Web interface

An interactive web interface for querying curated PPIs has been developed and is
available at:
[https://reproducible-interactome.nnet.fr](https://reproducible-interactome.genouest.org)
