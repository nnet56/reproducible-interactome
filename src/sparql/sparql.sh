#!/bin/bash

# sparql.sh
#
# Author: Marc Melkonian <marc.melkonian@etudiant.univ-rennes1.fr>
#
# Helper script for querying heterodimers triplestores.
# 
# Usage:
# -----
# sparql.sh <taxid (9606|559292)> <query.rq>
#
# Options:
# -------
# taxid         target organism, either 9606 (human) or 559292 (yeast)
# <query.rq>    SPARQL query file

# Apache JENA query executable path
EXE_PATH=$HOME/apache-jena-3.17.0/bin/tdb2.tdbquery
# Persistent store base pathname (generated by tdb2.tdbloader)
DB_PATH=$HOME/Inexplicit-rdd/data/triplestores/

if [[ $# < 2 ]]
then
    echo "usage: sparql.sh <taxid (9606|559292)> <query.rq>"
    exit 1
fi

$EXE_PATH \
    --results csv \
    --loc=${DB_PATH}/db_$1_NA \
    --query "$2"
